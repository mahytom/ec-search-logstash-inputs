package eu.wajja.web.fetcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.logstash.plugins.ConfigurationImpl;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.elastic.logstash.api.Configuration;

public class WebFetcherTest {

    private Properties properties;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void intialize() throws IOException {

       properties = new Properties();
       properties.load(this.getClass().getClassLoader().getResourceAsStream("blockchain-observatory.properties"));
    }

    @Test
    public void testWebFetcher() throws IOException {

        Map<String, Object> configValues = new HashMap<>();

        configValues.put(WebFetcher.CONFIG_URLS.name(), Arrays.asList(objectMapper.readValue((String) properties.get(WebFetcher.PROPERTY_URLS), String[].class)));
        configValues.put(WebFetcher.CONFIG_TIMEOUT.name(), new Long((String) properties.get(WebFetcher.PROPERTY_TIMEOUT)));
        configValues.put(WebFetcher.CONFIG_MAX_PAGES.name(), new Long((String) properties.get(WebFetcher.PROPERTY_MAX_PAGES)));

        configValues.put(WebFetcher.CONFIG_MAX_DEPTH.name(), new Long((String) properties.get(WebFetcher.PROPERTY_MAX_DEPTH)));

        configValues.put(WebFetcher.CONFIG_CHROME_DRIVERS.name(), Arrays.asList(objectMapper.readValue((String) properties.get(WebFetcher.PROPERTY_CHROME_DRIVERS), String[].class)));
        configValues.put(WebFetcher.CONFIG_REINDEX.name(), new Boolean((String) properties.get(WebFetcher.PROPERTY_REINDEX)));
        configValues.put(WebFetcher.CONFIG_ENABLE_CRAWL.name(), new Boolean((String) properties.get(WebFetcher.PROPERTY_ENABLE_CRAWL)));
        configValues.put(WebFetcher.CONFIG_READ_ROBOT.name(), new Boolean((String) properties.get(WebFetcher.PROPERTY_READ_ROBOT)));

        // user agent content
        configValues.put(WebFetcher.CONFIG_CRAWLER_REFERER.name(), properties.get(WebFetcher.PROPERTY_CRAWLER_REFERER));
        configValues.put(WebFetcher.CONFIG_CRAWLER_USER_AGENT.name(), properties.get(WebFetcher.PROPERTY_CRAWLER_USER_AGENT));

        // Exclusion of sites
        String excludeData = (String) parseJsonData((String) properties.get(WebFetcher.PROPERTY_EXCLUDE_DATA));
        String excludeLink = (String) parseJsonData((String) properties.get(WebFetcher.PROPERTY_EXCLUDE_LINK));
        String includeLink = (String) parseJsonData((String) properties.get(WebFetcher.PROPERTY_INCLUDE_LINK));

        configValues.put(WebFetcher.CONFIG_EXCLUDE_DATA.name(), getItemAsList(excludeData));
        configValues.put(WebFetcher.CONFIG_EXCLUDE_LINK.name(), getItemAsList(excludeLink));
        configValues.put(WebFetcher.CONFIG_INCLUDE_LINK.name(), getItemAsList(includeLink));
        
        // Elastic config
        configValues.put(WebFetcher.CONFIG_ELASTIC_HOSTNAMES.name(), Arrays.asList(objectMapper.readValue((String) properties.get(WebFetcher.PROPERTY_ELASTIC_HOSTNAMES), String[].class)));
        configValues.put(WebFetcher.CONFIG_ELASTIC_PASSWORD.name(), parseJsonData((String) properties.get(WebFetcher.PROPERTY_ELASTIC_PASSWORD)));
        configValues.put(WebFetcher.CONFIG_ELASTIC_USERNAME.name(), parseJsonData((String) properties.get(WebFetcher.PROPERTY_ELASTIC_USERNAME)));

        configValues.put(WebFetcher.CONFIG_ENABLE_REGEX.name(), new Boolean((String) properties.get(WebFetcher.PROPERTY_ENABLE_REGEX)));
        configValues.put(WebFetcher.CONFIG_ENABLE_DELETE.name(), new Boolean((String) properties.get(WebFetcher.PROPERTY_ENABLE_DELETE)));
        configValues.put(WebFetcher.CONFIG_ENABLE_JSLINKS.name(), true);

        Configuration config = new ConfigurationImpl(configValues);
        WebFetcher webFetcher = new WebFetcher("test-id", config, null);
        webFetcher.stopped = false;

        TestConsumer testConsumer = new TestConsumer();
        webFetcher.start(testConsumer);

        List<Map<String, Object>> events = testConsumer.getEvents();

        Assert.assertEquals(0, events.size());
        webFetcher.stop();
    }

    private List getItemAsList(String value) {
        List<String> result = Arrays.stream(value.split(","))
                .map(s -> (String)parseJsonData(s)) // Remove any leading or trailing quotes
                .collect(Collectors.toList());
        return result;
    }
    
    
    private Object parseJsonData(String value) {

        if (value.startsWith("\"") && value.endsWith("\"")) {
            return value.substring(1, value.length() - 1);
        }

        if (value.startsWith("[") && value.endsWith("]")) {

            value = value.replace("[\"", "");
            value = value.replace("\"]", "");
            value = value.replace("\",\"", ",");

            return value;
        }

        return value;
    }

    private static class TestConsumer implements Consumer<Map<String, Object>> {

        private List<Map<String, Object>> events = new ArrayList<>();

        @Override
        public void accept(Map<String, Object> event) {

            synchronized (this) {

                if (events.size() < 10) {
                    events.add(event);
                }
            }
        }

        public List<Map<String, Object>> getEvents() {

            return events;
        }
    }
    
    /**
     * 
     */
    		
    public void initConfigValues(Map<String, Object> configValues) {
    	configValues.put("urls",new ArrayList<>());
    	configValues.put("excludeData",new ArrayList<>());
    	configValues.put("excludeLink",new ArrayList<>());
    	configValues.put("includeLink",new ArrayList<>());
    	configValues.put("timeout",8000);
    	configValues.put("maxdepth",0);
    	configValues.put("maxpages",100);
    	configValues.put("refreshInterval",86400l);
    	configValues.put("proxyHost","");
    	configValues.put("proxyPort",80);
    	configValues.put("proxyUser","");
    	configValues.put("proxyPass","");
    	configValues.put("cron","");
    	configValues.put("sleep",1);
    	configValues.put("consumer","");
    	configValues.put("chromeDrivers",new ArrayList<>());
    	configValues.put(WebFetcher.CONFIG_CRAWLER_USER_AGENT.name(),WebFetcher.CONFIG_CRAWLER_USER_AGENT);
    	configValues.put(WebFetcher.CONFIG_CRAWLER_REFERER.name(),WebFetcher.CONFIG_CRAWLER_REFERER);
    	configValues.put("waitForCssSelector","");
    	configValues.put("maxWaitForCssSelector",30);
    	configValues.put("readRobot",true);
    	configValues.put("rootUrl","");
    	configValues.put("reindex",false);
    	configValues.put("enableCrawl",true);
    	configValues.put("enableDelete",true);
    	configValues.put("enableRegex",true);
    	configValues.put("enableHashtag",false);
    	configValues.put("enabledJsLinks",true);
    	configValues.put("ignoreHttpError",false);   	
    }
}
