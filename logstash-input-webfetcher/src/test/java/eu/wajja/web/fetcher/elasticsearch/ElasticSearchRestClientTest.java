package eu.wajja.web.fetcher.elasticsearch;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;

public class ElasticSearchRestClientTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchRestClientTest.class);

    // @Test
    public void httpConnectionTest() throws IOException {

        ElasticRestClient client = new ElasticRestClient(Arrays.asList("http_es:1200"), "user", "*****", null, null, null, null, null);
        RestHighLevelClient restHighLevelClient = client.restHighLevelClient();
        ClusterHealthResponse health = restHighLevelClient.cluster().health(new ClusterHealthRequest(), RequestOptions.DEFAULT);
        assertNotNull(health.getClusterName());
    }

    // @Test
    public void httpsConnectionTest() throws IOException {

        ElasticRestClient client = new ElasticRestClient(Arrays.asList("https://es_https.com"), "user", "*****", null, null, null, null, null);
        RestHighLevelClient restHighLevelClient = client.restHighLevelClient();
        ClusterHealthResponse health = restHighLevelClient.cluster().health(new ClusterHealthRequest(), RequestOptions.DEFAULT);
        assertNotNull(health.getClusterName());
    }

    @Test
    public void httpsConnection1Test() throws IOException {

        ElasticRestClient client = new ElasticRestClient(Arrays.asList("https://vpc-dev-xcv67oeckwlbqi5zxekios5ygi.eu-west-1.es.amazonaws.com"), "admin", "8!V63grEvbw8", null, null, null, null, null);
        RestHighLevelClient restHighLevelClient = client.restHighLevelClient();
        ClusterHealthResponse health = restHighLevelClient.cluster().health(new ClusterHealthRequest(), RequestOptions.DEFAULT);

        List<String> hostnames = Arrays.asList("https://vpc-dev-xcv67oeckwlbqi5zxekios5ygi.eu-west-1.es.amazonaws.com");
        String username = "admin";
        String password = "8!V63grEvbw8";
        String proxyscheme = null;
        String proxyHostname = null;
        Long proxyPort = null;
        String proxyUsername = null;
        String proxyPassword = null;

        ElasticSearchService elasticSearchService = new ElasticSearchService(hostnames, username, password, proxyscheme, proxyHostname, proxyPort, proxyUsername, proxyPassword);

        elasticSearchService.checkIndex("testindex");
        
        LOGGER.info(health.getClusterName());
        assertNotNull(health.getClusterName());
    }
}
