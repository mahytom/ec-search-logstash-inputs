package eu.wajja.web.fetcher.controller;

import org.junit.Assert;
import org.junit.Test;

public class ProxyControllerTest {

    public static final String PROXY_USER = "user";
    public static final String PROXY_PASS = "pass";
    public static final Long PROXY_PORT = 12345L;

    @Test
    public void httpsHostProxyTest() {
        String proxyHost = "https://host.com";

        ProxyController proxyController = new ProxyController(PROXY_USER, PROXY_PASS, proxyHost, PROXY_PORT);
        Assert.assertEquals("https", proxyController.getProxyScheme());
    }

    @Test
    public void httpHostProxyTest() {
        String proxyHost = "http://host.com";

        ProxyController proxyController = new ProxyController(PROXY_USER, PROXY_PASS, proxyHost, PROXY_PORT);
        Assert.assertEquals("http", proxyController.getProxyScheme());
    }

    @Test
    public void nullHostProxyTest() {
        String proxyHost = null;

        ProxyController proxyController = new ProxyController(PROXY_USER, PROXY_PASS, proxyHost, PROXY_PORT);
        Assert.assertNull(proxyController.getProxyScheme());
    }

    @Test
    public void justHostProxyTest() {
        String proxyHost = "host.com";

        ProxyController proxyController = new ProxyController(PROXY_USER, PROXY_PASS, proxyHost, PROXY_PORT);
        Assert.assertEquals("http", proxyController.getProxyScheme());
    }

}