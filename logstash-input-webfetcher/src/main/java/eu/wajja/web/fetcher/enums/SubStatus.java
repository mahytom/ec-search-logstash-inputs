package eu.wajja.web.fetcher.enums;

public enum SubStatus {

	included,
	excluded,
	excluded_metadata
}
